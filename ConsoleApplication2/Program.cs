﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            A a = new A();
            B b = new B(a);
            C c = new C(a);

            a.Raise("右");
            a.Raise("左");
            a.Fall();
        }
    }
        public delegate void RaiseEventHandler(string hand);
        public delegate void FallEventHandler();
    public class A
    {
        public event RaiseEventHandler RaiseEvent;
        public event FallEventHandler FallEvent;
        public void Raise(string hand)
        {
            Console.WriteLine("首領A{0}手舉杯", hand);
            if(RaiseEvent != null)
            {
                RaiseEvent(hand);
            }
        }
        public void Fall() {
            Console.WriteLine("首領A摔杯，兩路伏兵殺出");
            if(FallEvent != null)
            {
                FallEvent();
            }
        }

    }   
    public class B
    {
        A a;
        public B(A a)
        {
            this.a = a;
            a.RaiseEvent += new RaiseEventHandler(a_RaiseEvent);
            a.FallEvent += new FallEventHandler(a_FallEvent);
        }

       void a_RaiseEvent(string hand)
        {
            if (hand.Equals("左"))
            {
                attack();
            }
        }

        void a_FallEvent()
        {
            
            attack();
            
        }
        public void attack()
        {
            Console.WriteLine("B使用野蠻衝鋒發動攻擊");
        }
    }

    public class C
    {
        A a;
        public C(A a)
        {
            this.a = a;
            a.RaiseEvent += new RaiseEventHandler(a_RaiseEvent);
            a.FallEvent += new FallEventHandler(a_FallEvent);
        }

        void a_RaiseEvent(string hand)
        {
            if (hand.Equals("右"))
            {
                attack();
            }
        }

        void a_FallEvent()
        {

            attack();

        }
        public void attack()
        {
            Console.WriteLine("C帶領一隊大刀對殺出");
        }
    }
}
